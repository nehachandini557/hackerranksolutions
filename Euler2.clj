(use '[clojure.string :only (split triml)])

(
        let [
          t_t (read-line) 
          t (Integer/parseInt t_t) 
        ]

      (
        loop [a0 t]
        (when (> a0 0)
          (
            let [
              n_t (read-line) 
              n (Integer/parseInt n_t) 
            ]            
            
            (def fib-seq-seq
            ((fn fib [a b] (lazy-seq (cons a (fib b (+ a b))))) 0 1))
             (println (reduce + (filter even? (take-while #(< % n) fib-seq-seq))))
            
            
          )

        (recur (- a0 1) ) )
      )

)

