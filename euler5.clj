(use '[clojure.string :only (split triml)])

(
        let [
          t_t (read-line) 
          t (Integer/parseInt t_t) 
        ]

      (
        loop [a0 t]
        (when (> a0 0)

          (
            let [
              n_t (read-line) 
              n (Integer/parseInt n_t) 
              
            ]
            
            (let [num (+ n 1) ]
            (defn gcd [a b] (if (zero? b) a (recur b (mod a b))))  ; greatest common divisor
            (defn lcm [a b] (/ (* a b) (gcd a b)))                 ; lowest common multiple 
            (println (reduce #(lcm %1 %2) (range 1 num))))
            
          )

        (recur (- a0 1) ) )
      )

)

