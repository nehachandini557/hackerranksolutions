(use '[clojure.string :only (split triml)])

(
        let [
          t_t (read-line) 
          t (Integer/parseInt t_t) 
        ]

      (
        loop [a0 t]
        (when (> a0 0)

          (
            let [
              n_temp (read-line) 
              n_t (split n_temp #"\s+") 
              n (Integer/parseInt (n_t 0)) 
              k (Integer/parseInt (n_t 1)) 
            ]
          

          (
            let [
              num (read-line)  
            ]
            (defn get-partitions [] (partition k 1 (str num)))
            (defn char->int [c] (- (int c) (int \0)))
             (println (->> (get-partitions)
       (map #(apply * (map char->int %)))
       (apply max)))

        )
          )

        (recur (- a0 1) ) )
      )

)
