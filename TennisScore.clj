(def gamex (atom 0))
(def gamey (atom 0))
(def setx (atom 0))
(def sety (atom 0))
(def cx (atom 0))
(def cy (atom 0))

(defn gameSet [x y]
  (if(and ( = x 6) (< y 5))
   (swap! setx inc))
  (if (and (= y 6) (< x 5))
    (swap! sety inc))
)

(defn game [cx cy]
  (if (= cx 4)
      (swap! gamex inc)
      (swap! gamey inc)
    )
  (gameSet @gamex @gamey)
  )

(defn currGame [lst]
  (doseq [n lst]
    (if (= n "x") (swap! cx inc)
      (swap! cy inc)
    )
(if (or (= 4 @cx) (= 4 @cy))
      (do
        (game @cx @cy)
        (reset! cx 0 )
      (reset! cy 0 ))
)))

(defn score []
  (def chances "xyxxxxxxx")
  (def lst (re-seq #".{1,1}" chances))
  (currGame lst)
  (print @setx)
  (print "-->")
  (println @sety)
  (print @gamex)
  (print "-->")
  (println @gamey)
  (print @cx)
  (print "-->")
  (println @cy)
  )
(score)                                                                                                                44,6          Bot

