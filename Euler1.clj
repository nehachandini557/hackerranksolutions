(use '[clojure.string :only (split triml)])

(
        let [
          t_t (read-line) 
          t (Integer/parseInt t_t) 
        ]

      (
        loop [a0 t]
        (when (> a0 0)

          (
            let [
              n_t (read-line) 
              n (Integer/parseInt n_t) 
            ]
            (println (reduce + (set (concat (range 0 n 3) (range 0 n 5)))))
          )

        (recur (- a0 1) ) )
      )

)
